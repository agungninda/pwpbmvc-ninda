<?php

require_once('../app/core/Controller.php');

class Login extends Controller {
    public function index() {
        $data["title"] = "Login";
        $data["msg"] = "";
        $this->view('auth/login', $data);
    }

    public function storeLogin() {
        if($this->model('UserModel')->findUserBy($_POST["username"], $_POST["username"])) {
            $loggedUser = $this->model('UserModel')->login($_POST);

            if($loggedUser) {
                $this->model('UserModel')->createSession($loggedUser);
            } else {
                $data["title"] = "Login";
                $data["msg"] = "Kata Sandi Salah!";
                $this->view('auth/login', $data);
            }
        } else {
            $data["title"] = "Login";
            $data["msg"] = "Username tidak di temukan.";
            $this->view('auth/login', $data);
        }
    }
}

?>