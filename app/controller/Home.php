<?php

require_once('../app/core/Controller.php');

class Home extends Controller {

    public function index() {
        $data['title'] = 'Home';
        $data['name'] = $this->model('UserModel')->getAllUser();
        $this->view('template/header', $data);
        $this->view('home/index', $data);
        $this->view('template/footer', $data);
    }
}

?>