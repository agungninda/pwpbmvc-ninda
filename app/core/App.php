<?php
class App {
    protected   $controller = "Home",
                $method = "index",
                $params = array();

    public function __construct() {
        $url = $this->parseUrl();
        
        if(isset($url[0]) && file_exists("../app/controller/" . $url[0] . ".php")) {
            $this->controller = $url[0];
            require_once("../app/controller/" . $url[0] . ".php");
            unset($url[0]);
        }
        $this->controller = new $this->controller;

        if(isset($url[1]) && method_exists($this->controller, $url[1])) {
            $this->method = $url[1];
            unset($url[1]);
        }
        $this->params = array_values($url);

        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function parseUrl() {
        if($_GET['url']) {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $urls = explode('/', $url);
            return $urls;
        }
    }
}